<?php
namespace MiniBC\addons\ordermanagesystem;

use MiniBC\core\Auth;
use MiniBC\core\Config;
use MiniBC\core\controller\ControllerManager;
use MiniBC\core\entities\Addon;
use MiniBC\core\route\Route;

// define('APP_PATH', dirname(__FILE__)."/storefront/app");

/**
 * Router for Affiliate Only Addon
 *
 * @uses MiniBC\core\Auth
 * @uses MiniBC\core\Config
 * @uses MiniBC\core\entities\Addon
 * @uses MiniBC\core\controller\ControllerManager
 *
 * @extends MiniBC\core\route\Route
 */
class AddonRoute extends Route {

	public $basePath = '';
	public $name = '';
	public $label = '';
	public $addon = null;

	protected $customerPath;
	/**
	 * setup routes for this addon
	 * @param 	MiniBC\core\entities\Addon 	$addon instance of the addon object
	 */


	public function __construct(Addon $addon)
	{

		$this->name = $addon->name;
		$this->addon = $addon;

		// setup paths
		$customerBasePath = Config::get('routes::customer');
		$adminBasePath = Config::get('routes::admin');
		$addonBasePath = Config::get('routes::addon');
		$this->basePath = $addonBasePath . '/ordermanagesystem';
		$this->customerPath = $customerBasePath . $this->basePath;

		$authorizeController = ControllerManager::get('Authorize@ordermanagesystem');
		$orderController = ControllerManager::get('Order@ordermanagesystem');
		// $emailController = ControllerManager::get('Email@ordermanagesystem');

		$this->get($this->customerPath . '/searchOrder', array($orderController, 'searchOrder'));

		// Authorize Controller
		// $this->post($this->customerPath . '/authorizeOrders', array($authorizeController, 'authorizeOrders'));

		// $this->post($this->customerPath . '/saveAuthList', array($authorizeController, 'saveAuthList'));
    //
		// $this->post($this->customerPath . '/adjustAuthItem', array($authorizeController, 'adjustAuthItem'));
    //
		// $this->get($this->customerPath . '/pmAuthlists', array($authorizeController, 'getAuthLists'));
    //
		// $this->get($this->customerPath . '/pmAuthlistsEdits' . '/{id:.+}', array($authorizeController, 'getAuthEditLists'));
    //
		// $this->post($this->customerPath . '/cancelAuthItem', array($authorizeController, 'cancelAuthItem'));
    //
		// $this->post($this->customerPath . '/cancelAll', array($authorizeController, 'cancelAllItems'));

		$this->post($this->customerPath . '/captureProducts', array($authorizeController, 'captureProducts'));
		$this->post($this->customerPath . '/getCreditCardInfo', array($authorizeController, 'getCreditCardInfo'));

		// $this->get($this->customerPath . '/exportAuthReport', array($authorizeController, 'exportAuthReport'));
    //
		// $this->get($this->customerPath . '/exportCaptureReport', array($authorizeController, 'exportCaptureReport'));

		// Email Controller
		// $this->post($this->customerPath . '/sendEmails', array($emailController, 'sendEmails'));
    //
		// $this->get($this->customerPath . '/getEmailTemplates', array($emailController, 'getEmailTemplates'));
    //
		// $this->get($this->customerPath . '/getEmailTemplate'. '/{id:.+}', array($emailController, 'getEmailTemplate'));
    //
		// $this->post($this->customerPath . '/saveEmailTemplate'. '/{id:.+}', array($emailController, 'saveEmailTemplate'));
    //
		// $this->post($this->customerPath . '/saveEmailParts', array($emailController, 'saveEmailParts'));
    //
		// $this->get($this->customerPath . '/getEmailSettings', array($emailController, 'getEmailSettings'));
    //
		// $this->post($this->customerPath . '/saveEmailsettings', array($emailController, 'saveEmailsettings'));
    //
		// $this->get($this->customerPath . '/SMTPTesting', array($emailController, 'SMTPTesting'));
	}

	protected function methodNotAllowed() {
		// 405 Method Not Allowed
		http_response_code(405);
		exit;
	}

	protected function routeNotFound() {
		// 404 Not Found
		http_response_code(404);
		exit;
	}
}
