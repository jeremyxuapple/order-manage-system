<?php
namespace MiniBC\addons\ordermanagesystem\controllers;

use \DateTime;
use Bigcommerce\Api\Client;
use MiniBC\core\Auth;
use MiniBC\core\connection\ConnectionManager;
use MiniBC\core\entities\Addon;
use MiniBC\core\Mail;
use MiniBC\core\mail\Message;
use MiniBC\core\EntityFactory;
use MiniBC\bigcommerce\services\StorefrontAssetsService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use MiniBC\core\services\exception\WebDAVConnectionFailedException;
// use MiniBC\addons\ordermanagesystem\services\EmailService;

class OrderController
{
    private $db = null;
    private $addon = null;
    private $categories = null;
    private $limit = null;
    private $customer = null;
    private $store = null;
    private $apiConnection = null;
    private $email_template_path;
    private $storecolor;
    private $webDavService = false;

    public function __construct()
    {
        $this->db = ConnectionManager::getInstance('mysql');
        $this->customer = Auth::getInstance()->getCustomer();
        $this->store = $this->customer->stores[0];
    }

    /**
    * Search order
    *
    * @param
    * @param
    * @return JsonResponse|Response
    */

    public function searchOrder()
    {
      $customer_store_id = $this->customer->id;

      if (isset($_GET['term'])) {
          $term = $_GET['term'];
          // When there is a search term passed in, we will make a new query base on the term
          $orders_query = "
          SELECT o.bc_id AS order_id, o.status, CONCAT_WS(' ', o.billing_first_name, o.billing_last_name) AS customer_name, o.total_inc_tax
          FROM bigbackup_bc_orders o
          WHERE o.customer_store_id = $customer_store_id
          AND c.first_name LIKE '%$term%'
          OR c.last_name LIKE '%$term%'
          OR c.email LIKE '%$term%'
          OR o.bc_id LIKE '%$term%'
          ";

          $search_query = "
              SELECT o.bc_id AS order_id, o.status, o.bc_customer_id, CONCAT_WS(' ', o.billing_first_name, o.billing_last_name) AS customer_name, o.billing_email, op.quantity, op.sku, op.name
              FROM bigbackup_bc_orders o
              LEFT JOIN bigbackup_bc_orders_products op
              ON op.order_id = o.bc_id
              WHERE o.customer_id = $customer_store_id
              AND op.customer_id = $customer_store_id
              AND o.bc_id = $orderID";

          $orders = $this->db->query($orders_query);

          $result["pointsOrders"] = $orders;
          header('Content-Type: text/json');
          echo json_encode($result);
          exit();

      } else {
          // When the page first load, no terms passed in
          $orders_query = "
          SELECT o.bc_id AS order_id, o.status,
            CONCAT_WS(' ', o.billing_first_name, o.billing_last_name) AS customer_name,
            FORMAT(o.total_inc_tax, 2) AS total_inc_tax,
            SUBSTRING(date_created, 6, 11) AS date_created
          FROM bigbackup_bc_orders o
          WHERE o.customer_id = $customer_store_id";

          $orders = $this->db->query($orders_query);

          if(empty($id)) {
              return JsonResponse::create($orders);
              exit();
              } else {

                  // foreach ($orders as $order) {
                  //     if($order["id"] == $id) {
                  //
                  //     }
                  // }
              }
          }

    }

    /**
    * Charge confirm order
    *
    * @param
    * @param
    * @return JsonResponse|Response
    */

    public function authorizeConfirm()
    {
        $orders = $_GET['orders'];
        $productName = $_GET['productName'];

        $customer_store_id = $this->customer->id;
        $selected_orders = array();

        foreach($orders as $id) {

            $selected_orders_query = "
                SELECT o.bc_id AS id, CONCAT_WS(' ', o.billing_first_name, o.billing_last_name) AS customer_name,
                    pp.gateway_data, FORMAT(o.total_inc_tax, 2) AS total
                FROM bigbackup_bc_orders o
                LEFT JOIN bigbackup_bc_orders_products p
                    ON o.customer_id = p.customer_id
                    AND o.bc_id = p.order_id
                LEFT JOIN rc_customer_profiles cp
                    ON o.bc_customer_id = cp.store_customer_id
                LEFT JOIN rc_payment_profiles pp
                    ON cp.profile_id = pp.customer_profile_id
                WHERE o.bc_id = $id
                AND o.customer_id = $customer_store_id
                AND p.name = '$productName'
            ";

            $order = $this->db->queryFirst($selected_orders_query);

            $gateway_data = unserialize($order['gateway_data']);
            $last4 = $gateway_data['last4'];
            $expiry = $gateway_data['expiry'];

            unset($order['gateway_data']);
            $order['last4'] = $gateway_data['last4'];
            $order['expiry'] = $gateway_data['expiry'];

            array_push($selected_orders, $order);
        }

        return JsonResponse::create($selected_orders);
    }

    /**
    * Export the result from the search
    *
    */

    public function exportResults($rows)
    {
        $header = array('Order', 'Order Status', 'Customer Name', 'Customer Email', 'Card On File', 'Quantity', 'SKU', 'Product Name');

        // send file header
        header("Content-Type: text/csv;charset=utf-8");
        header("Content-Disposition: attachment; filename=file.csv");
        header("Pragma: no-cache");
        header("Expires: 0");

        // write CSV to output stream
        $output = fopen("php://output", "w");

        // write header
        fputcsv($output, $header);

        foreach ($rows as $row) {
             $searchRow = array(
                'order_id' => $row['order_id'],
                'status' => $row['status'],
                'customer_name' => $row['customer_name'],
                'billing_email' => $row['billing_email'],
                'cardOnFile' => $row['cardOnFile'],
                'quantity' => $row['quantity'],
                'sku' => $row['sku'],
                'name' => $row['name'],
            );
            fputcsv($output, $searchRow); // here you can change delimiter/enclosure
        }

        fclose($output);
        exit;
    }

    /**
    * Verify if the card in on the file
    */
    public function verifyCardOnFile($customerStoreID, $customerId)
    {
        $query = "
                SELECT rpp.gateway_data
                FROM rc_customer_profiles rcp
                LEFT JOIN rc_payment_profiles rpp
                    ON rcp.profile_id = rpp.customer_profile_id
                    AND rcp.customer_id = rpp.customer_id
                WHERE rcp.customer_id = $customerStoreID
                AND rcp.store_customer_id = $customerId
                ";

        $profile = $this->db->queryFirst($query);

        return $profile['gateway_data'] != null ? true : false;
    }

    /**
    * Build proudct name list for options
    */
    public function searchProductNames()
    {
        $customer_id = $this->customer->id;
        $productNameQuery = "SELECT bc_id AS id, name AS label FROM bigbackup_bc_products WHERE customer_id = $customer_id";

        $productNames = $this->db->query($productNameQuery);
        return JsonResponse::create($productNames);

    }

}
