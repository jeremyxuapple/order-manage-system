<?php
namespace MiniBC\addons\ordermanagesystem\controllers;

use \DateTime;
use Bigcommerce\Api\Client;
use MiniBC\core\Auth;
use MiniBC\core\Log;
use MiniBC\core\connection\ConnectionManager;
use MiniBC\core\entities\Addon;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use MiniBC\addons\recurring\services\PaymentService;
use MiniBC\addons\recurring\services\PaymentProfileService;

class AuthorizeController {
		private $db = null;
    private $addon = null;
    private $categories = null;
    private $limit = null;
    private $customer = null;
    private $store = null;
    private $apiConnection = null;
    private $email_template_path;
    private $storecolor;
    private $webDavService = false;

    public function __construct()
    {
        $this->db = ConnectionManager::getInstance('mysql');
        $this->customer = Auth::getInstance()->getCustomer();
        $this->store = $this->customer->stores[0];
    }

  /**
  * Capture Products
  */

  public function captureProducts()
  {
    $customer_store_id = $this->customer->id;

    $customer_id = $_POST['customer_id'];
    $products = $_POST['products'];
		$capturedList = array();

    foreach ($products as $product) {
			$orderId = $product['orderId'];
      $amount = $product['total'];

      $paymentProfileQuery = "SELECT rpp.id, rpp.gateway, rpp.gateway_data
                              FROM rc_payment_profiles rpp
                              JOIN bigbackup_bc_orders o
                              JOIN rc_customer_profiles rcp
                                  ON rcp.store_customer_id = o.bc_customer_id
                                  AND rcp.profile_id = rpp.customer_profile_id
                              WHERE rcp.store_customer_id = $customer_id
                              AND o.customer_id = $customer_store_id
                              AND rpp.customer_id = $customer_store_id
                              AND rcp.customer_id =$customer_store_id
                              ";
      $paymentData = $this->db->queryFirst($paymentProfileQuery);
			$gateway_data = unserialize($paymentData['gateway_data']);

			$captureInfo = array();
      // $baseInfo = $this->getCustomerInformation($orderId, $productName);

      try {
  			$gateway = PaymentService::getInstance()->getGateway($paymentData['gateway'], $this->store);

  			$profile = PaymentProfileService::getInstance()->getFromId($paymentData['id'], $this->store);

  			// set gateway to authorize only mode
  			$gateway->paymentTransactionType = 'authCaptureTransaction';

  			// set metadata for the gateway
  			$metadata = array('orderId' => $orderId);

  			// create transaction
  			$response = $gateway->createTokenizedPayment($amount, $profile, $metadata);

				$captureInfo['product_id'] = $product['productId'];
        $captureInfo['transactionId'] = $response->getTransactionId();
        $captureInfo['gateway'] = $paymentData['gateway'];
				$captureInfo['gateway_data'] = $gateway_data;

  			if ($response->getStatus() == 'payment_success') {
          $captureInfo['payment_status'] = 'Success';
  			} else {
  				$captureInfo['payment_status'] = 'Failed';
  			}
        	$captureInfo['message'] = $response->getMessage();
  		} catch (\Exception $e) {
  			// handle exception here
  		}
			array_push($capturedList, $captureInfo);
    } // End of the for loop

		if (sizeof($capturedList) == 0) return JsonResponse::create(array('error' => 'Unexpected Error'));
    return JsonResponse::create($capturedList);
  }


    /**
    * get the credit card information base on orderId
    */

    public function getCreditCardInfo($orderId)
    {
        $customerStoreId = $this->customer->id;
				$orderId = $_POST['orderId'];
        $query = "SELECT pp.gateway_data
								FROM bigbackup_bc_orders o
								LEFT JOIN rc_customer_profiles cp
										ON o.bc_customer_id = cp.store_customer_id
								LEFT JOIN rc_payment_profiles pp
										ON cp.profile_id = pp.customer_profile_id
								WHERE o.customer_id = $customerStoreId
								AND o.bc_id = $orderId
                ";

        $info = $this->db->queryFirst($query);
        $gateway_data = unserialize($info['gateway_data']);

				return JsonResponse::create($gateway_data);

    }

}
